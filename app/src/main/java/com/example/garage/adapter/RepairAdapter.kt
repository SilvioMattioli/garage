package com.example.garage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.garage.R
import com.example.garage.databaseClasses.Repair

class RepairAdapter(private val list: ArrayList<Repair>, private val context: Context) :
    RecyclerView.Adapter<RepairAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItem(repair: Repair) {
            val description: TextView = itemView.findViewById(R.id.tv_description)
            val quantity: TextView = itemView.findViewById(R.id.tv_quantity)
            val price: TextView = itemView.findViewById(R.id.tv_price)
            val guarantee: TextView = itemView.findViewById(R.id.tv_guarantee)
            val total: TextView = itemView.findViewById(R.id.tv_total)
            description.text = repair.name.toString()
            quantity.text = repair.stock.toString()
            price.text = repair.price.toString()
            if (repair.guarantee){
                guarantee.text = "ja"
                total.text = "0.00"
            }
            else{
                guarantee.text = "nee"
                total.text = (repair.stock!!.toInt() * repair.price!!.toDouble()).toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): RepairAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_layout_repair, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RepairAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}