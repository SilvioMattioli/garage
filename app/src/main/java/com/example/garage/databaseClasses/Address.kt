package com.example.garage.databaseClasses

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Address(street:String,streetNr:String, city:String,postalCode:String) {
    var street = street
    var streetNr = streetNr
    var city = city
    var postalCode = postalCode

}