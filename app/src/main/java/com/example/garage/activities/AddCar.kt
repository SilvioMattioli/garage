package com.example.garage.activities

import android.content.Intent
import android.os.Bundle
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.example.garage.R
import com.example.garage.menu.Factory
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.content_add_car.*

class AddCar : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var clientId: String
    private var factory: Factory = Factory()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_car)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        clientId = intent.getStringExtra("CLIENT")

        btn_addCar.setOnClickListener {
            factory.insertCar(
                ed_nr.text.toString().trim(),
                ed_brand.text.toString().trim(),
                ed_model.text.toString().trim(),
                ed_color.text.toString().trim(),
                ed_key.text.toString().trim(),
                ed_co2.text.toString().trim(),
                ed_version.text.toString().trim(),
                ed_motor.text.toString().trim(),
                clientId
            )
            var i = Intent(this, Clients::class.java)
            this.startActivity(i)
        }

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                var i = Intent(this, Clients::class.java)
                startActivity(i)
            }
            R.id.nav_planning -> {
                var i = Intent(this, Calendar::class.java)
                startActivity(i)
            }
            R.id.nav_addClient -> {
                var i = Intent(this, AddClient::class.java)
                startActivity(i)
            }
            R.id.nav_stock -> {
                var i = Intent(this, Stock::class.java)
                startActivity(i)
            }
            R.id.nav_signout->{
                FirebaseAuth.getInstance().signOut()
                var i = Intent(this, Login::class.java)
                startActivity(i)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}
