package com.example.garage.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.garage.R
import com.example.garage.databaseClasses.Repair
import com.example.garage.menu.Factory
import com.google.firebase.firestore.*
import kotlinx.android.synthetic.main.activity_add_repair.*
import kotlinx.android.synthetic.main.content_add_repair.*
import kotlinx.android.synthetic.main.pieces.*
import kotlinx.android.synthetic.main.pieces.view.*

class AddRepair : AppCompatActivity() {
    private var factory: Factory = Factory()
    private lateinit var clId: String
    private var listC: ArrayList<String> = arrayListOf()
    private val db = FirebaseFirestore.getInstance()
    private lateinit var price:String
    private lateinit var product:String
    private lateinit var quantity:String

    @SuppressLint("InflateParams")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_repair)
        setSupportActionBar(toolbar)
        price = "0"
        quantity = "0"
        clId = intent.getStringExtra("CLIENT")
        listC = factory.getProductName()
        factory.retrieveCars(clId,this,rec_car)
        factory.retrieveRepairs(rec_repair,this, clId)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listC)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            val mDialog = LayoutInflater.from(this).inflate(R.layout.pieces, null)

            mDialog.edit_quantity.value = 0
            mDialog.edit_quantity.minValue = 0
            mDialog.edit_quantity.maxValue = 99
            mDialog.spinner.adapter = adapter

            mDialog.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                     product = parent!!.getItemAtPosition(position).toString()
                    val collQ = db.collection("Objects").whereEqualTo("name", product)
                    collQ.get().addOnSuccessListener { querySnapshot ->
                        var list = querySnapshot.documents
                        println("silvio:${list.size}")
                        for (l in list){
                             price = l["price"].toString()
                            mDialog.ed_total.text = (price.toDouble() * mDialog.edit_quantity.value).toString()
                        }
                        mDialog.price.text = price
                    }
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            }
            mDialog.edit_quantity.setOnValueChangedListener { picker, oldVal, newVal ->
                mDialog.ed_total.text = (price.toDouble() * newVal).toString()
                quantity = newVal.toString()
            }
            mDialog.dia_checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                 if(isChecked){
                    mDialog.ed_total.text ="0.00"
                } else{
                     mDialog.ed_total.text = (price.toDouble() * mDialog.edit_quantity.value).toString()
                }
            }
            val builder = AlertDialog.Builder(this@AddRepair)
            builder.setTitle("Afspraken toevoegen")
                .setView(mDialog)
            val dialog: AlertDialog = builder.create()
            dialog.show()
            dialog.ad_Repair.setOnClickListener {
                var repair = Repair("",product,quantity.toInt(),price,mDialog.dia_checkbox.isChecked,clId)
                factory.insertRepair(
                    repair
                )
                dialog.dismiss()
                var i = Intent(this, AddRepair::class.java)
                i.putExtra("CLIENT",clId)
                startActivity(i)
                this@AddRepair.finish()
            }
            dialog.ad_cancel_edit.setOnClickListener {
                dialog.dismiss()
            }
        }
    }


}
