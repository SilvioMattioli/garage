package com.example.garage.menu

import android.content.Context
import android.content.Intent
import android.widget.GridView
import androidx.recyclerview.widget.RecyclerView
import com.example.garage.activities.Calendar
import com.example.garage.activities.Clients
import com.example.garage.adapter.*
import com.example.garage.databaseClasses.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query

class Factory {

    private val db = FirebaseFirestore.getInstance()

    fun retrieveProducts(recyclerView: RecyclerView, context: Context) {
        var cAdapter: AdapterStock
        var layoutManager:RecyclerView.LayoutManager?
        var list: ArrayList<Product> = arrayListOf()
        var coll = db.collection("Objects")
        coll?.get()?.addOnSuccessListener { documents ->
            for (d in documents) {
                var product = Product(
                    d.id,
                    d.data["name"].toString(),
                    d.data["stock"].toString().toInt(),
                    d.data["price"].toString(),
                    d.data["ref"].toString()

                )
                list.add(list.size, product)
            }
            if (list.isNotEmpty()) {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                cAdapter = AdapterStock(list, context)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = cAdapter

            }
        }
    }

    fun insertProduct(product: Product) {
        var coll = db.collection("Objects")
        coll.add(product)
    }

    fun deletePrduct(prId: String) {
        var coll = db.collection("Objects").document(prId)
        coll.delete().addOnSuccessListener {
        }
    }

    fun retrieveRepairs(recyclerView: RecyclerView, context: Context,clientId:String) {
        var layoutManager: RecyclerView.LayoutManager?
        var list: ArrayList<Repair> = arrayListOf()
        var coll = db.collection("Repairs").whereEqualTo("clientId", clientId)
        coll?.get()?.addOnSuccessListener { documents ->
            for (d in documents) {
                var repair = Repair(
                    d.id,
                    d.data["name"].toString(),
                    d.data["stock"].toString().toInt(),
                    d.data["price"].toString(),
                    d.data["guarantee"].toString().toBoolean(),
                    d.data["clientId"].toString()

                )
                list.add(list.size, repair)
            }
            if (list.isNotEmpty()) {

                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                var repairAdapter = RepairAdapter(list, context)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = repairAdapter
            }
        }
    }

    fun insertRepair(repair: Repair) {
        var coll = db.collection("Repairs")
        coll.add(repair)
    }
    
    fun deleteAppointment(appId: String) {
        var coll = db.collection("Appointments").document(appId)
        coll.delete().addOnSuccessListener {
        }
    }

    fun retrieveClientsGrid(gridview: GridView, context: Context) {
        var gv = gridview
        var cAdapter: ClAdapter
        var coll = db.collection("Clients")
        var list: ArrayList<Client> = arrayListOf()
        coll?.get()?.addOnSuccessListener { documents ->
            for (d in documents) {
                var client = Client(
                    d.id,
                    d.data["name"].toString(),
                    d.data["firstName"].toString(),
                    null,
                    d.data["phone"].toString(),
                    d.data[""].toString(),
                    d.data["cellPhone"].toString(),
                    d.data["email"].toString(),
                    d.data["btw"].toString(),
                    d.data[""].toString(),
                    d.data["uri"].toString()
                )
                list.add(list.size, client)
            }
            if (list.isNotEmpty()) {

                cAdapter = ClAdapter(list, context)
                gv.adapter = cAdapter
            }
        }
    }

    fun retieveClients(): ArrayList<String> {
        var listC: java.util.ArrayList<String> = arrayListOf()
        val collC = db.collection("Clients").orderBy("name", Query.Direction.ASCENDING)
        collC.get().addOnSuccessListener { documents ->
            for (d in documents) {
                val client = Client(
                    d.id,
                    d.data["name"].toString(),
                    d.data["firstName"].toString(),
                    null,
                    d.data["phone"].toString(),
                    d.data[""].toString(),
                    d.data["cellPhone"].toString(),
                    d.data["email"].toString(),
                    d.data["btw"].toString(),
                    d.data[""].toString(),
                    d.data["uri"].toString()
                )
                listC.add(listC.size, (client.firstName + " " + client.name))
            }
            if (listC.isNotEmpty()) {
            }
        }
        return listC
    }

    fun retrieveClientsFilter(name: String, gridview: GridView, context: Context) {
        var gv = gridview
        var cAdapter: ClAdapter
        var list: ArrayList<Client> = arrayListOf()
        val collQ = db.collection("Clients").orderBy("name").startAt(name).endAt("$name\uf8ff")
        collQ.get()?.addOnSuccessListener { documents ->
            for (d in documents) {
                var client = Client(
                    d.id,
                    d.data["name"].toString(),
                    d.data["firstName"].toString(),
                    null,
                    d.data["phone"].toString(),
                    d.data[""].toString(),
                    d.data["cellPhone"].toString(),
                    d.data["email"].toString(),
                    d.data["btw"].toString(),
                    d.data[""].toString(),
                    d.data["uri"].toString()
                )
                list.add(list.size, client)
            }
            if (list.isNotEmpty()) {
                cAdapter = ClAdapter(list, context)
                gv.adapter = cAdapter
            }
        }
    }

    fun retrieveAppointments(recyclerView:RecyclerView, dateAp: String, context: Context): ArrayList<Appointment> {
        var appAdapter: AppAdapter
        var list: ArrayList<Appointment> = arrayListOf()
        var layoutManager: RecyclerView.LayoutManager?
        val coll = db.collection("Appointments").whereEqualTo("date", dateAp).orderBy("hour", Query.Direction.ASCENDING)
        coll.get().addOnSuccessListener { documents ->
            for (d in documents) {
                var appointment = Appointment(
                    d.id,
                    d.data["date"].toString(),
                    d.data["hour"].toString(),
                    d.data["info"].toString(),
                    d.data["clientName"].toString()

                )
                list.add(list.size, appointment)
            }
            if (list.isNotEmpty()) {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                appAdapter = AppAdapter(list, context)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = appAdapter
            } else {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                appAdapter = AppAdapter(list, context)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = appAdapter
            }
        }
        return list

    }

    fun insertAppointment(appId:String?,dateAp: String, hour: String, info: String, client: String) {
        var collA = db.collection("Appointments")
        val appointment = Appointment(appId,dateAp, hour, info, client)
        collA!!.add(appointment).addOnSuccessListener {

        }

    }

    fun insertCar(nr: String, brand: String, model: String, color: String?, key: String?, co2: String, version: String, motor: String, clientId: String) {

        var coll = db.collection("Clients").document(clientId).collection("Cars")
        var car = Car("", nr, brand, model, color, key, co2, version, motor)
        coll!!.add(car).addOnSuccessListener {

        }

    }

    fun insertClient(name: String, firstName: String, address: Address, phone: String?, fax: String?, cellPhone: String, email: String, btw: String, language: String, uri: String) {
        val coll = db.collection("Clients")
        var client = Client("", name, firstName, address, phone, fax, cellPhone, email, btw, language, uri)
        coll!!.add(client).addOnSuccessListener {

        }

    }

    fun retrieveCars(clId: String, context: Context, recyclerView: RecyclerView) {
        val coll = db.collection("Clients").document(clId).collection("Cars")
        var list: ArrayList<Car> = arrayListOf()
        var layoutManager: RecyclerView.LayoutManager?
        var cAdapter: CarAdapter
        coll?.get()?.addOnSuccessListener { documents ->
            for (d in documents) {
                var car = Car(
                    d.id,
                    d.data["nr"].toString(),
                    d.data["brand"].toString(),
                    d.data["model"].toString(),
                    d.data["color"].toString(),
                    d.data["key"].toString(),
                    d.data["co2"].toString(),
                    d.data["version"].toString(),
                    d.data["motor"].toString()
                )
                list.add(list.size, car)
            }
            if (list.isNotEmpty()) {
                layoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
                cAdapter = CarAdapter(list, context)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = cAdapter

            }
        }
    }

    fun getProductName(): ArrayList<String> {
        var listC: ArrayList<String> = arrayListOf()
        var collP = db.collection("Objects").orderBy("name", Query.Direction.ASCENDING)
        collP?.get()?.addOnSuccessListener { documents ->
            for (d in documents) {
                var product = Product(
                    d.id,
                    d.data["name"].toString(),
                    d.data["stock"].toString().toInt(),
                    d.data["price"].toString(),
                    d.data["ref"].toString()

                )
                listC.add(listC.size, product.name.toString())
            }
        }
        return listC
    }
}