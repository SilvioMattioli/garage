package com.example.garage.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import com.example.garage.R
import com.example.garage.databaseClasses.Client
import com.example.garage.menu.Factory
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.content_clients.*

class Clients : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private var factory: Factory = Factory()
    private var list: ArrayList<Client> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_clients)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        factory.retrieveClientsGrid(gridview, this)


        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        val menuItem: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {

        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        if (newText!!.isEmpty()) {
            factory.retrieveClientsGrid(gridview, this)
        } else {
            factory.retrieveClientsFilter(newText.toLowerCase(), gridview, this)
        }
        return false
    }

    @SuppressLint("PrivateResource")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                var i = Intent(this, Clients::class.java)
                finish()
                startActivity(i)
                overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top)
            }
            R.id.nav_planning -> {
                var i = Intent(this, Calendar::class.java)
                startActivity(i)
            }
            R.id.nav_addClient -> {
                var i = Intent(this, AddClient::class.java)
                startActivity(i)
            }
            R.id.nav_stock -> {
                var i = Intent(this, Stock::class.java)
                startActivity(i)
            }
            R.id.nav_signout->{
                FirebaseAuth.getInstance().signOut()
                var i = Intent(this, Login::class.java)
                startActivity(i)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}
