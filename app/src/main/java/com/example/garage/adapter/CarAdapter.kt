package com.example.garage.adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.garage.R
import com.example.garage.databaseClasses.Car

class CarAdapter(private val list: ArrayList<Car>, private val context: Context) :
    RecyclerView.Adapter<CarAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItem(car: Car) {
            val nr: TextView = itemView.findViewById(R.id.tv_nr)
            val brand: TextView = itemView.findViewById(R.id.tv_brand)
            val model: TextView = itemView.findViewById(R.id.tv_model)
            var image: ImageView = itemView.findViewById(R.id.imageView)

            nr.text = "Nummer Plaat: ${car.nr} "
            brand.text = "Merk: ${car.brand}"
            model.text = "Model: ${car.model}"
            image.setImageResource(R.drawable.ic_menu_camera)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CarAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_layout_car, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CarAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}