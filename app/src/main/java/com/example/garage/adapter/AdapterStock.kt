package com.example.garage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.example.garage.R
import com.example.garage.activities.Stock
import com.example.garage.databaseClasses.Product
import com.example.garage.menu.Factory
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.stock_dialog.*
import kotlinx.android.synthetic.main.stock_dialog.view.*

class AdapterStock (private val list: ArrayList<Product>, private val context: Context) :
    RecyclerView.Adapter<AdapterStock.ViewHolder>() {
    private var factory: Factory = Factory()
    private val db = FirebaseFirestore.getInstance()
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindItem(product: Product) {
            val name: TextView = itemView.findViewById(R.id.tv_name)
            val stock: TextView = itemView.findViewById(R.id.tv_stock)
            val price: TextView = itemView.findViewById(R.id.tv_price)
            val ref: TextView = itemView.findViewById(R.id.tv_ref)
            val delete: ImageButton = itemView.findViewById(R.id.delete_product)
            val edit: ImageButton = itemView.findViewById(R.id.edit_product)
            name.text = product.name
            stock.text = product.stock.toString()
            price.text = product.price
            ref.text = product.ref

            delete.setOnClickListener {
                factory.deletePrduct(product.id.toString())
                val intent = Intent(context, Stock::class.java)
                context.startActivity(intent)
            }
           edit.setOnClickListener {
               val mDialog = LayoutInflater.from(context).inflate(R.layout.stock_dialog, null)
               val builder = AlertDialog.Builder(context)
               builder.setTitle("Edit Stock ${product.name}")
                   .setView(mDialog)

               mDialog.ad_stock.visibility = (View.GONE)
               mDialog.edit_stock.visibility = (View.VISIBLE)

               mDialog.ed_ref.setText(product.ref)
               mDialog.ed_description.setText(product.name)
               mDialog.ed_price.setText(product.price)
               mDialog.edit_quantity.setText(product.stock.toString())

               val dialog: AlertDialog = builder.create()
               dialog.show()
               dialog.edit_stock.setOnClickListener {

                   var productNew=
                       Product(
                           "",
                           mDialog.ed_description.text.toString(),
                           mDialog.edit_quantity.text.toString().toInt(),
                           mDialog.ed_price.text.toString(),
                           mDialog.ed_ref.text.toString()
                       )
                   var coll = db.collection("Objects").document(product.id!!)
                   coll.set(productNew).addOnSuccessListener {
                   }.addOnFailureListener{
                       Toast.makeText(context,"Edit niet gelukt,check uw internet verbinding",Toast.LENGTH_LONG).show()
                   }
                   var i = Intent(context,Stock::class.java)
                   context.startActivity(i)
               }
               dialog.ad_cancel_edit.setOnClickListener {
                   dialog.dismiss()
               }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): AdapterStock.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_layout_stock, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AdapterStock.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}