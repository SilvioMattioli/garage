package com.example.garage.databaseClasses

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Car(
    var id:String?,
    var nr: String?,
    var brand: String?,
    var model: String?, var color: String?, var key: String?,
    var co2: String, var version: String?, var motor: String?
)