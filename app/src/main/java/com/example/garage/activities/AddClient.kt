package com.example.garage.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.example.garage.R
import com.example.garage.databaseClasses.Address
import com.example.garage.menu.Factory
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_add_client.*
import kotlinx.android.synthetic.main.content_addclient.*
import java.io.ByteArrayOutputStream

@Suppress("PLUGIN_WARNING", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddClient : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    private var factory: Factory = Factory()
    private lateinit var fullBitmap: Bitmap
    private lateinit var mAuth: FirebaseAuth
    private val ALL_PERMISSIONS = 101
    private var currentUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_client)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)


        mAuth = FirebaseAuth.getInstance()
        var conf = Bitmap.Config.ARGB_8888
        fullBitmap = Bitmap.createBitmap(1, 1, conf)

        val permissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        ActivityCompat.requestPermissions(this, permissions, ALL_PERMISSIONS)
        btn_insertData.setOnClickListener {

            val fullname = ed_name.text.toString() + ed_firstname.text.toString()
            val address = Address(
                ed_street.text.toString().trim(),
                ed_number.text.toString().trim(),
                ed_city.text.toString().trim(),
                ed_postalcode.text.toString().trim()
            )
            uploadImageToFirebaseStorage(fullBitmap, fullname.toLowerCase(), address)
        }
        btn_insert_image.setOnClickListener {
            val callCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (callCameraIntent.resolveActivity(packageManager) != null) {
                startActivityForResult(callCameraIntent, 0)
            }
        }

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    public override fun onStart() {
        super.onStart()
        currentUser = mAuth.currentUser
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }



    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_home -> {
                var i = Intent(this, Clients::class.java)
                startActivity(i)
            }
            R.id.nav_planning -> {
                var i = Intent(this, Calendar::class.java)
                startActivity(i)
            }
            R.id.nav_addClient -> {
                var i = Intent(this, AddClient::class.java)
                finish()
                startActivity(i)
                overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top)
            }
            R.id.nav_stock -> {
                var i = Intent(this, Stock::class.java)
                startActivity(i)
            }
            R.id.nav_signout->{
                FirebaseAuth.getInstance().signOut()
                var i = Intent(this, Login::class.java)
                startActivity(i)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            0 -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    btn_insert_image.setImageBitmap(data.extras.get("data") as Bitmap)
                    fullBitmap = data.extras.get("data") as Bitmap
                }
            }
            else -> {
                Toast.makeText(this, "niet gelukt", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun uploadImageToFirebaseStorage(bitmap: Bitmap, clientName: String, address: Address) {
        val baos = ByteArrayOutputStream()
        var uriX: String
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()
        val ref = FirebaseStorage.getInstance().getReference("/images/$clientName.jpg")
        ref.putBytes(data).addOnSuccessListener { it ->
            Log.d("save image", "Succesfully uploaded image:${it.metadata}")
            ref.downloadUrl.addOnSuccessListener {
                uriX = (if (bitmap.height == 1) {
                    ""
                } else {
                    it
                }).toString()
                factory.insertClient(
                    ed_name.text.toString().trim(),
                    ed_firstname.text.toString().trim(),
                    address,
                    ed_phone.text.toString().trim(),
                    ed_fax.text.toString().trim(),
                    "",
                    ed_email.text.toString().trim(),
                    ed_Btw_Number.text.toString().trim(),
                    "",
                    uriX
                )
                var i = Intent(this, Clients::class.java)
                this.startActivity(i)
            }

        }


    }


}
