package com.example.garage.databaseClasses

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Product(var id: String?,
              var name: String?,
              var stock:Int?,
              var price:String?,
              var ref:String?)