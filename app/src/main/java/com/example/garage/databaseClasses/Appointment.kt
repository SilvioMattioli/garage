package com.example.garage.databaseClasses

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Appointment (var id:String?,
                   var date:String?,
                   var hour: String?,
                   var info: String?,
                   var clientName:String
                   )