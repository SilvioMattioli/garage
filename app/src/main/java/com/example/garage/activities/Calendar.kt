package com.example.garage.activities

import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.garage.R
import com.example.garage.databaseClasses.Appointment
import com.example.garage.menu.Factory
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.appointment_dialog.view.*
import kotlinx.android.synthetic.main.content_calendar.*
import kotlinx.android.synthetic.main.pieces.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class Calendar : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


   private var format: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
    private var dateIL = com.example.garage.menu.Menu(format.format(LocalDate.now()))
    private var list: ArrayList<Appointment> = arrayListOf()
    private  var date: String = dateIL.dateI
    private var factory: Factory = Factory()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calendar)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        list = factory.retrieveAppointments(rec_calendar,date,this)
        var adapter =  ArrayAdapter(this,android.R.layout.simple_spinner_item,factory.retieveClients())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)


        calendar.setOnDateChangeListener { view, year, month, dayOfMonth ->
            var m = "0${month+1}"
            date = if (month <10){
                "$dayOfMonth/$m/$year"
            } else{
                "$dayOfMonth/${month + 1}/$year"
            }
            list.clear()
            list = factory.retrieveAppointments(rec_calendar,date,this)
        }


        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            var client=""

            val mDialog = LayoutInflater.from(this).inflate(R.layout.appointment_dialog, null)
            mDialog.time.setIs24HourView(true)
            val builder = AlertDialog.Builder(this@Calendar)
            builder.setTitle("Afspraken toevoegen op: $date")
                .setView(mDialog)
            val dialog: AlertDialog = builder.create()
            mDialog.spinner.adapter = adapter
            mDialog.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                 client = parent!!.getItemAtPosition(position).toString()
                }
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }
            }
            dialog.show()
            dialog.ad_Appointment.setOnClickListener {
                val cTime = mDialog.time.hour.toString() + ":" + mDialog.time.minute.toString()
                factory.insertAppointment("",date, cTime, mDialog.ad_description.text.toString(),client)
                val i = Intent(this, Calendar::class.java)
                this.startActivity(i)
            }
            dialog.ad_cancel_edit.setOnClickListener {
                dialog.dismiss()
            }
        }



        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                var i = Intent(this, Clients::class.java)
                startActivity(i)
            }
            R.id.nav_planning -> {
                var i = Intent(this, Calendar::class.java)
                finish()
                startActivity(i)
                overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top)
            }
            R.id.nav_addClient -> {
                var i = Intent(this, AddClient::class.java)
                startActivity(i)
            }
            R.id.nav_stock -> {
                var i = Intent(this, Stock::class.java)
                startActivity(i)
            }
            R.id.nav_signout->{
                FirebaseAuth.getInstance().signOut()
                var i = Intent(this, Login::class.java)
                startActivity(i)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }




}
