package com.example.garage.databaseClasses

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Client(
    var id: String?,
    var name: String?,
    var firstName: String?,
    var address: Address?, var phone: String?, var fax: String?,
    var cellPhone: String, var email: String?, var btw: String?, var language: String?,var uri:String?
)
