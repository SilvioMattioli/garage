package com.example.garage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.example.garage.R
import com.example.garage.activities.Calendar
import com.example.garage.databaseClasses.Appointment
import com.example.garage.menu.Factory
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.appointment_dialog.*
import kotlinx.android.synthetic.main.appointment_dialog.view.*
import kotlinx.android.synthetic.main.appointment_dialog.view.edit_appointment
import kotlinx.android.synthetic.main.stock_dialog.ad_cancel_edit

class AppAdapter (private val list: ArrayList<Appointment>, private val context: Context) :
    RecyclerView.Adapter<AppAdapter.ViewHolder>() {
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var factory: Factory = Factory()
        private val db = FirebaseFirestore.getInstance()
        @SuppressLint("SetTextI18n")
        fun bindItem(appointment: Appointment) {
            val hour: TextView = itemView.findViewById(R.id.tv_hour)
            val description: TextView = itemView.findViewById(R.id.tv_description)
            val client: TextView = itemView.findViewById(R.id.tv_client)
            val delete: ImageButton = itemView.findViewById(R.id.delete_appointment)
            val edit: ImageButton = itemView.findViewById(R.id.edit_appointment)
            hour.text = appointment.hour
            description.text = appointment.info
            client.text = appointment.clientName

            delete.setOnClickListener {
                factory.deleteAppointment(appointment.id.toString())
                val intent = Intent(context, Calendar::class.java)
                context.startActivity(intent)
            }


            var adapter =  ArrayAdapter(context,android.R.layout.simple_spinner_item,factory.retieveClients())
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            edit.setOnClickListener {
                val mDialog = LayoutInflater.from(context).inflate(R.layout.appointment_dialog, null)
                val builder = AlertDialog.Builder(context)
                builder.setTitle("Edit Appointment ${appointment.hour}")
                    .setView(mDialog)

                mDialog.ad_Appointment.visibility = (View.GONE)
                mDialog.edit_appointment.visibility = (View.VISIBLE)

                mDialog.ad_description.setText(appointment.info)

                val dialog: AlertDialog = builder.create()
                var client = ""

                mDialog.spinner.adapter = adapter
                mDialog.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
                    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        client = parent!!.getItemAtPosition(position).toString()
                    }
                    override fun onNothingSelected(parent: AdapterView<*>?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }
                }
                dialog.show()
                dialog.edit_appointment.setOnClickListener {
                    var min= ""
                    min = if(mDialog.time.minute < 10){
                        "0${mDialog.time.minute}"
                    } else{
                        mDialog.time.minute.toString()
                    }
                    var appointment2=
                        Appointment(
                            "",
                            appointment.date,
                            mDialog.time.hour.toString() + ":" + min,
                            mDialog.ad_description.text.toString(),
                            client
                        )
                    var coll = db.collection("Appointments").document(appointment.id!!)
                    coll.set(appointment2).addOnSuccessListener {
                    }.addOnFailureListener{
                        Toast.makeText(context,"Edit niet gelukt,check uw internet verbinding", Toast.LENGTH_LONG).show()
                    }
                    var i = Intent(context, Calendar::class.java)
                    context.startActivity(i)
                }
                dialog.ad_cancel_edit.setOnClickListener {
                    dialog.dismiss()
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): AppAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.card_layout_appointment, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: AppAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }
}