package com.example.garage.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.garage.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_login.*

class Login : AppCompatActivity() {
    private lateinit var mAuth: FirebaseAuth
    private var currentUser: FirebaseUser? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()

        Picasso.get().load(R.mipmap.garage_mattioli).into(imageView_login)

        imageView_login.setOnClickListener {
            val rotateAnimation = RotateAnimation(
                0f, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )

            rotateAnimation.interpolator = LinearInterpolator()
            rotateAnimation.duration = 2000
            imageView_login.startAnimation(rotateAnimation)
        }

        btn_login.setOnClickListener {
            if (controle()) {
                signIn(ed_userName.text.toString(), ed_password.text.toString())
            }
        }
        btn_register.setOnClickListener {
            if (controle()) {
                createAccount(ed_userName.text.toString(), ed_password.text.toString())
            }
        }
    }

    public override fun onStart() {
        super.onStart()
        currentUser = mAuth.currentUser
    }

    private fun createAccount(email: String, password: String) {
        Log.d("creating", "createAccount:$email")

        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("userCreated", "createUserWithEmail:success")
                    currentUser = mAuth.currentUser
                } else {
                    Log.w("Failed", "createUserWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Password more than 6 char",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    private fun signIn(email: String, password: String) {
        Log.d("signin", "signIn:$email")

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.d("", "signInWithEmail:success")
                    currentUser = mAuth.currentUser
                    var i = Intent(this, Clients::class.java)
                    i.putExtra("UID", currentUser?.uid.toString())
                    startActivity(i)
                } else {
                    Log.w("", "signInWithEmail:failure", task.exception)
                    Toast.makeText(
                        baseContext, "Wrong email or Password",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
    }

    private fun controle(): Boolean {
        if (ed_userName.text.trim().isEmpty()) {
            ed_userName.error = "Email moet ingevuld worden"
            ed_userName.requestFocus()
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(ed_userName.text.toString()).matches()) {
            ed_userName.error = "Gelieve een email in te vullen"
            ed_userName.requestFocus()
            return false
        }
        if (ed_password.text.trim().isEmpty()) {
            ed_password.error = "Password moet ingevuld worden"
            ed_password.requestFocus()
            return false
        }
        if (ed_password.text.length < 6) {
            ed_password.error = "Password moet groter dan 6 charaters zijn"
            ed_password.requestFocus()
            return false
        }
        return true

    }


}
