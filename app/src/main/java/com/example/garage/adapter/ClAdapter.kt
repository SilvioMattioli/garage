package com.example.garage.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.garage.R
import com.example.garage.activities.AddCar
import com.example.garage.activities.AddRepair
import com.example.garage.databaseClasses.Client
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.card_layout.view.*
import kotlinx.android.synthetic.main.content_login.*

class ClAdapter(private val list: ArrayList<Client>?, context: Context) : BaseAdapter() {

    private var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private lateinit var card: View
    private var c = context
    @SuppressLint("ViewHolder", "SetTextI18n")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        card = inflater.inflate(R.layout.card_layout, null)
        val name: TextView = card.findViewById(R.id.tv_naam)
        val email: TextView = card.findViewById(R.id.tv_email)
        val btw: TextView = card.findViewById(R.id.tv_btw)
        val phone: TextView = card.findViewById(R.id.tv_tel)
        var image: CircleImageView = card.findViewById(R.id.imageView_client)
        val repair = card.btn_repairs
        val cars = card.btn_addCar

        name.text = "Naam: ${list?.get(position)?.firstName} " + " ${list?.get(position)?.name}"
        email.text = "Email: ${list?.get(position)?.email}"
        btw.text = "BTW: ${list?.get(position)?.btw}"
        phone.text = "tel: ${list?.get(position)?.phone}"


        image.setOnClickListener {
            val rotateAnimation = RotateAnimation(
                0f, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f
            )

            rotateAnimation.interpolator = LinearInterpolator()
            rotateAnimation.duration = 2000
            image.startAnimation(rotateAnimation)
        }
        if (list!![position].uri == ""){
            image.setImageResource(R.drawable.ic_menu_camera)
        }else{
            Picasso.get().load(list!![position].uri).into(image)
        }

        repair.setOnClickListener {
            val intent = Intent(c, AddRepair::class.java)
            intent.putExtra("CLIENT", list?.get(position)?.id)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            c.startActivity(intent)
        }
        cars.setOnClickListener {
            val intent = Intent(c, AddCar::class.java)
            intent.putExtra("CLIENT", list?.get(position)?.id)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            c.startActivity(intent)
        }


        return card
    }

    override fun getItem(position: Int): Any? {
        return null
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        if (list != null) {
            return list.size
        }
        return 0
    }


}

