package com.example.garage.activities

import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.LayoutInflater
import android.view.MenuItem
import com.example.garage.R
import com.example.garage.databaseClasses.Product
import com.example.garage.menu.Factory
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.content_stock.*
import kotlinx.android.synthetic.main.pieces.ad_cancel_edit
import kotlinx.android.synthetic.main.stock_dialog.*
import kotlinx.android.synthetic.main.stock_dialog.view.*

class Stock : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var factory: Factory = Factory()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock)
        val toolbar: Toolbar = findViewById(R.id.toolbar_stock)
        setSupportActionBar(toolbar)
        factory.retrieveProducts(rec_stock, this)
        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener {
            val mDialog = LayoutInflater.from(this).inflate(R.layout.stock_dialog, null)


            val builder = AlertDialog.Builder(this@Stock)
            builder.setTitle("Stock Toevoegen")
                .setView(mDialog)
            val dialog: AlertDialog = builder.create()
            dialog.show()

            dialog.ad_stock.setOnClickListener {
                factory.insertProduct(
                    Product(
                        "",
                        mDialog.ed_description.text.toString(),
                        mDialog.edit_quantity.text.toString().toInt(),
                        mDialog.ed_price.text.toString(),
                        mDialog.ed_ref.text.toString()
                    )
                )
                var i = Intent(this,Stock::class.java)
                startActivity(i)
            }
            dialog.ad_cancel_edit.setOnClickListener {
                dialog.dismiss()
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
               val intent = Intent(this, Clients::class.java)
               intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
               startActivity(intent)

        }
    }




    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_home -> {
                var i = Intent(this, Clients::class.java)
                startActivity(i)
            }
            R.id.nav_planning -> {
                var i = Intent(this, Calendar::class.java)
                startActivity(i)
            }
            R.id.nav_addClient -> {
                var i = Intent(this, AddClient::class.java)
                startActivity(i)
            }
            R.id.nav_stock -> {
                var i = Intent(this, Stock::class.java)
                finish()
                startActivity(i)
                overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_top)
            }
            R.id.nav_signout->{
                FirebaseAuth.getInstance().signOut()
                var i = Intent(this, Login::class.java)
                startActivity(i)
            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

}
